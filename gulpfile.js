const   gulp = require('gulp'),
        sass = require('gulp-sass'),
        autoprefixer = require('gulp-autoprefixer'),
        pug = require('gulp-pug');

gulp.task('sass',()=>
    gulp.src('./assets/scss/*.scss')
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments:true
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            versions:['last 2 browsers'],
        }))
        .pipe(gulp.dest('./assets/css/'))
);

gulp.task('pug',()=>
    gulp.src('./views/pug/*.pug')
        .pipe(pug({
            pretty:true
        }))
        .pipe(gulp.dest('./dist'))
);

gulp.task('default',()=>{
   gulp.watch('./assets/scss/*.scss',['sass']);
   gulp.watch('./views/pug/**/*.pug',['pug']);
});